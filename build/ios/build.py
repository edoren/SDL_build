#!/usr/bin/env python3

import argparse
import os.path
import platform
import shutil
import subprocess

script_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="Build project")
parser.add_argument(
    "--build-type",
    choices=["Release", "Debug"],
    dest="build_type",
    default="Debug",
    help="The build type, Release or Debug")

args = parser.parse_args()

lib_output = os.path.join(script_dir, "output", args.build_type)

cmake_args = [
    # CMake generator
    "-G", "Xcode",
    "-DCMAKE_TOOLCHAIN_FILE={}".format(os.path.join(script_dir, "ios.toolchain.cmake")),
    "-DPLATFORM={}".format("OS64COMBINED"),
    "-DDEPLOYMENT_TARGET=13.0",
    # CMake config
    "-DCMAKE_BUILD_TYPE={}".format(args.build_type),
    "-DCMAKE_INSTALL_PREFIX='{}'".format(lib_output),
    "--no-warn-unused-cli",
    # Platform config
    # Library config
    "-DSDL_STATIC=TRUE",
    "-DSDL_STATIC_PIC=TRUE",
    "-DSDL_SHARED=FALSE",
    "-DSDL_AUDIO_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_RENDER_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_THREADS_ENABLED_BY_DEFAULT=FALSE",
    "-DSDL_TIMERS_ENABLED_BY_DEFAULT=FALSE",
]

if __name__ == "__main__":
    cmake_path = shutil.which("cmake")
    source_dir = os.path.join(script_dir, "..", "..", "sdl")
    # Generate project
    generate_command = [cmake_path, source_dir] + cmake_args
    print("============================================================")
    print(" ".join(generate_command))
    print("============================================================")
    subprocess.run(generate_command, cwd=script_dir, check=True)
    # Execute build
    build_command = [cmake_path, "--build", script_dir, "--target", "install",
                     "--config", args.build_type]
    print("============================================================")
    print(" ".join(build_command))
    print("============================================================")
    subprocess.run(build_command, cwd=script_dir, check=True)
    # Copy include to library output
    include_path = os.path.join(lib_output, "include")
    output_file_path = os.path.join(include_path, "SDL_config.h")
    shutil.move(os.path.join(include_path, "SDL2", "SDL_config.h"),
                output_file_path)
    # Fix SDL_config.h file
    with open(output_file_path, "r+", encoding="utf-8") as f:
        filedata = f.read()
        filedata = filedata.replace("#include \"SDL_platform.h\"", "#include <SDL2/SDL_platform.h>")
        f.seek(0)
        f.write(filedata)
    # Fix SDL_config.h import in include folder
    for dname, dirs, files in os.walk(include_path):
        for fname in files:
            fpath = os.path.join(dname, fname)
            _, fextension = os.path.splitext(fpath)
            if fextension in (".c", ".h"):
                with open(fpath, "r+", encoding="utf-8") as f:
                    filedata = f.read()
                    filedata = filedata.replace("#include \"SDL_config.h\"", "#include <SDL2/SDL_config.h>")
                    f.seek(0)
                    f.write(filedata)
