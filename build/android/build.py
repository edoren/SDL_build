#!/usr/bin/env python3

import argparse
import os.path
import platform
import subprocess

script_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="Build project")
parser.add_argument(
    "--build-type",
    choices=["Release", "Debug"],
    dest="build_type",
    default="Debug",
    help="The build type, Release or Debug")

args = parser.parse_args()

if __name__ == "__main__":
    # Execute build
    if platform.system() == "Windows":
        gradle_executable = "gradlew.bat"
    else:
        gradle_executable = "gradlew"
    gradle_path = os.path.join(script_dir, gradle_executable)
    build_command = [gradle_path, "assemble{}".format(args.build_type)]
    print("============================================================")
    print(" ".join(build_command))
    print("============================================================")
    subprocess.run(build_command, cwd=script_dir, check=True)
